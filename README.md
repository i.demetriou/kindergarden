COVID-19 daily check list automation
====================================

Automate the procedure of sending COVID-19 daily check list


Requirements
------------

- Ghostscript's `ps2pdf` is available. You can install it via

	sudo apt install ghostscript

- Email is sent via [mutt](http://www.mutt.org/). It is assumed that you have
  a working configuration


Modifying
---------

## Templates

The following customizations are currently available:

- Child's name
- Parent's name
- Parent's email
- Class: Could be one of Opal, Jade, or whatever

You can edit the appropriate parameter in the script.

## Email

You can edit the format of the sent email.

- Recipient (edit the `SEND_TO` variable in the script)
- Body (edit the `greetings.txt` file)
