#!/usr/bin/env bash
#
# Update a template word document with today's date, and send email to the
# kindergarten.
#
# Requirements:
#
# - Ghostscript's `ps2pdf` is available. You can install it via
#
#		sudo apt install ghostscript
#
# - Email is sent via [mutt](http://www.mutt.org/). It is assumed that you have
#   a working configuration

SCRIPT_NAME=$0
ROOT=`readlink -f "${SCRIPT_NAME}" | xargs dirname`

# [Template Customization] {{{
# TEMPLATE="${ROOT}/template.ps"
TEMPLATE="${ROOT}/template-original.ps"
## Placeholders
# CHILDNAME="%CHILDNAME%"
# PARENTNAME="%PARENTNAME%"
# EMAIL="%EMAIL%"
# CLASS="%CLASS%"
# DATE="%DATE%"
CHILDNAME="Nicholas Demetriou"
PARENTNAME="Ioannis Demetriou"
EMAIL="john.demetriou@gmail.com"
CLASS="Opal"
DATE=$(date +'%d-%m-%Y')

out="${CHILDNAME// /-}-daily-check-list-${DATE}"

generate_daily_check_list()
{
	sed -e "s/%CHILDNAME%/${CHILDNAME}/" \
        -e "s/%PARENTNAME%/${PARENTNAME}/" \
        -e "s/%EMAIL%/${EMAIL}/" \
        -e "s/%CLASS%/${CLASS}/" \
        -e "s/%DATE%/${DATE}/" ${TEMPLATE} > out.ps
	ps2pdf out.ps "${out}.pdf"
}
# [Template Customization] }}}

# [Email] {{{
SEND_TO="eva.littlegemslarnaca@gmail.com"
SUBJECT="Daily COVID-19 Check list for ${CHILDNAME}"
BODY="${ROOT}/greetings.txt"

termux_share_report()
{
	EXTRA_EMAIL=${SEND_TO}
	EXTRA_SUBJECT=${SUBJECT}
	EXTRA_TEXT="$(< ${BODY})"
	termux_share -a send -t "${EXTRA_SUBJECT}" ${1}
}

# Use this on a machine with MUTT installed
send_via_email()
{
	mutt -s "${SUBJECT}" -a "${out}.pdf" -- ${SEND_TO} < "${BODY}"
}
# [Email] }}}

temp=`mktemp -d`
pushd ${temp}
generate_daily_check_list
send_via_email
popd
rm -r ${temp}
